// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "BoidManager.h"
#include "Engine/World.h"
#include "TheElderGameMode.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Player/Mokosh.h"
#include "Components/SphereComponent.h"

ABoidManager::ABoidManager(){
	HasSpawned = false;
}

void ABoidManager::BeginPlay()
{
	Super::BeginPlay();
	
	InitialHoming();
}

void ABoidManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasSpawned) SetHoming();
	else InitialHoming();
	
}

void ABoidManager::InitialHoming()
{
	Movement->HomingTargetComponent = SpawnTarget->GetRootComponent();
}

void ABoidManager::TriggerBoids()
{
	if (HasSpawned) return;
	//Movement->HomingAccelerationMagnitude = 1000;
	SetCurrentTarget();
	SpawnBoids();
}

void ABoidManager::SetHoming()
{
	if (CurrentTarget < 0) return;
	for (int i = 0; i < boidBoys.Num(); i++)
	{
		boidBoys[i]->Movement->HomingTargetComponent = Targets[CurrentTarget]->GetRootComponent();
	}

	Movement->HomingTargetComponent = Targets[CurrentTarget]->GetRootComponent();
}

void ABoidManager::SetCurrentTarget()
{
	CurrentTarget++;
	if (CurrentTarget > (Targets.Num() - 1)) CurrentTarget = Targets.Num() - 1;
}

void ABoidManager::ResetTarget()
{
	Movement->HomingTargetComponent = nullptr;
}

void ABoidManager::SpawnBoids()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Owner = this;
	// get location of spawner
	FVector Location = GetActorLocation();
	// get a random location around spawner
	FRotator Rotation = GetActorRotation();


	for (int i = 0; i < BoidCount; i++) {
		FRotator newRot = Rotation;
		newRot.Yaw = FMath::RandRange(-89, 89);
		newRot.Pitch = FMath::RandRange(-89, 89);
		ABoidLeaf* newBoy =  GetWorld()->SpawnActor<ABoidLeaf>(BoidSpawn, Location, newRot, SpawnInfo);
		boidBoys.Add(newBoy);
	}
	HasSpawned = true;
}
