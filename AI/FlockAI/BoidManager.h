// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AI/BoidLeaf.h"
#include "BoidManager.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API ABoidManager : public ABoidLeaf
{
	GENERATED_BODY()
	

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boids")
		int BoidCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boids")
		bool ShouldSpawnBoids;

	// get actor class to spawn
	UPROPERTY(EditAnywhere, Category = "Boids")
		TSubclassOf<ABoidLeaf> BoidSpawn;

	UPROPERTY(EditAnywhere, Category = "Targets")
		TArray<AActor*> Targets;

	UPROPERTY(EditAnywhere, Category = "SpawnTrigger")
		AActor* SpawnTarget;

	UPROPERTY()
		FTimerHandle TargetTimer;
protected:

	UPROPERTY()
		TArray<ABoidLeaf*> boidBoys;
	
	UPROPERTY()
		class AMokosh* PlayerTarget;

	UPROPERTY()
		int CurrentTarget = -1;

	bool HasSpawned;

public:
	ABoidManager();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	void InitialHoming();

	UFUNCTION(BlueprintCallable)
	void TriggerBoids();
	
	void SetHoming();

	UFUNCTION(BlueprintCallable)
	void SetCurrentTarget();

	UFUNCTION(BlueprintCallable)
	void ResetTarget();
	
	void SpawnBoids();
};
