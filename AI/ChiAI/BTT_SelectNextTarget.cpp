// Fill out your copyright notice in the Description page of Project Settings.


#include "BTT_SelectNextTarget.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Player/Mokosh.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

EBTNodeResult::Type UBTT_SelectNextTarget::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) {
	// Follow mokosh
	AMokosh* playerRef = Cast<AMokosh>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	OwnerComp.GetBlackboardComponent()->SetValueAsVector("TargetLoc", playerRef->GetActorLocation());


	// move away from wall or ledge
	// TODO: come back to this if Chi is being annoying
	return EBTNodeResult::Succeeded;
}