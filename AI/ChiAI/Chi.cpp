// Fill out your copyright notice in the Description page of Project Settings.
#include "Chi.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"
#include "Camera/CameraComponent.h"
#include "Camera/ElderSpringArmComponent.h"
#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Player/CatapultComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/Engine.h"
#include "Animations/ChiAnimInstance.h"
#include "AI/ChiAI/ChiAIController.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"

AChi::AChi() {
	// accurate metric from doc
	GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed;
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);

	// player rotation not affected by the camera
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;


	CatapultLoadPoint = CreateDefaultSubobject<USceneComponent>(TEXT("CatapultLoadPoint"));
	//CatapultLoadPoint->SetupAttachment(RootComponent);
	
	
	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<UElderSpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	// ADDED
	CameraBoom->bUsePawnControlRotation = false;

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm


	// catapult spline variables
	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComp"));
	SceneComp->SetupAttachment(RootComponent);

	Spline = CreateDefaultSubobject<USplineComponent>(TEXT("Spline"));
	Spline->SetupAttachment(SceneComp);

	// register aim particle 
	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particle(TEXT("/Game/ArtAssets/Effects/Cascade/Emmiters/Mechanic/P_CatapultDestination"));
	CatapultParticle = Particle.Object;

	LastAimPos = FVector(0, 0, 0);

	CheckSpeed = true;
	CatapultReady = false;
	SpeedMult = 1;
	GenerateCatapultSpline = false;
	CatapultPitchValue = 50;

	PrimaryActorTick.bCanEverTick = true;
}

void AChi::OnConstruction(const FTransform& Transform)
{
	SplineMeshes.Empty();
	for (int i = 0; i <= 1; i++)
	{
		auto SplineMesh = NewObject<USplineMeshComponent>(this, USplineMeshComponent::StaticClass());
		
		if (MeshForSpline != nullptr)
		{
			SplineMesh->SetStaticMesh(MeshForSpline);
		}
		SplineMesh->RegisterComponent();
		SplineMesh->CreationMethod = EComponentCreationMethod::SimpleConstructionScript;
		SplineMesh->SetMobility(EComponentMobility::Movable);
		SplineMesh->ForwardAxis = ESplineMeshAxis::Z;
		FVector StartLocation;
		FVector StartTangent;
		FVector EndLocation;
		FVector EndTangent;
		Spline->GetLocationAndTangentAtSplinePoint(i, StartLocation, StartTangent, ESplineCoordinateSpace::Local);
		Spline->GetLocationAndTangentAtSplinePoint(i + 1, EndLocation, EndTangent, ESplineCoordinateSpace::Local);
		SplineMesh->SetStartAndEnd(StartLocation, StartTangent, EndLocation, EndTangent);
		//SplineMesh->SetWorldScale3D(FVector(.05, .05, 1));
		SplineMesh->AttachToComponent(Spline, FAttachmentTransformRules::FAttachmentTransformRules(EAttachmentRule::KeepRelative, true));
		SplineMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		SplineMesh->SetCollisionResponseToAllChannels(ECR_Ignore);

		// set visibility instead
		SplineMesh->SetVisibility(false);
		if (SplineMeshMaterial != nullptr)
		{
			SplineMesh->SetMaterial(0, SplineMeshMaterial);
		}
		SplineMeshes.Add(SplineMesh);
	}
	
}

void AChi::BeginPlay() {
	Super::BeginPlay();

	// cache references to player, aicontroller and playercontroller
	PlayerRef = Cast<AMokosh>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	ChiAIControllerRef = Cast<AChiAIController>(GetController());
	ChiPlayerController = UGameplayStatics::GetPlayerController(GetWorld(),0);

	// moved because of cook failure
	CatapultLoadPoint->RegisterComponent();

	auto CurrentAnimInstance = Cast<UChiAnimInstance>(GetMesh()->GetAnimInstance());
	if (CurrentAnimInstance != nullptr)
	{
		AnimInstance = CurrentAnimInstance;
	}

	params.TraceTag = TEXT("Position Test");
	params.bTraceComplex = true;
	params.AddIgnoredActor(this);
	params.AddIgnoredActor(PlayerRef);


	if(TransformActorToSpawn != nullptr)
	{
		ChiTransformActor = GetWorld()->SpawnActor<AActor>(TransformActorToSpawn, GetActorLocation(), GetActorRotation());
		ChiTransformActor->SetActorHiddenInGame(true);
	}

	// spawn particle and set to invisible
	LastParticle = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CatapultParticle, FVector(0, 0, 0));
	LastParticle->SetVisibility(false);

	SetActorTickEnabled(true);
}

void AChi::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	
	if (CheckSpeed) {
		SetWalkSpeed();
	}

	if(IsTransforming)
	{
		LerpParticle();
	}
}

void AChi::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);

	// movement inputs
	PlayerInputComponent->BindAxis("Horizontal", this, &AChi::MoveHorizontal);
	PlayerInputComponent->BindAxis("Vertical", this, &AChi::MoveVertical);

	// when catapult is ready, chi can rotate to choose direction
	PlayerInputComponent->BindAxis("RHorizontal", this, &AChi::TurnAtRate);
	PlayerInputComponent->BindAxis("RVertical", this, &AChi::LookUpAtRate);

	// launchplayer
	PlayerInputComponent->BindAction("CatapultLoad", IE_Pressed, this, &AChi::ActivateCatapult);

	// cancel
	PlayerInputComponent->BindAction("Cancel", IE_Pressed, PlayerRef->CatapultComponent, &UCatapultComponent::CancelCatapult);

}

void AChi::OnPossess()
{
	
}

float AChi::Remap(float val, FVector2D xy1, FVector2D xy2)
{
	return (val - xy1.X) / (xy1.Y - xy1.X) * (xy2.Y - xy2.X) + xy2.X;
}

// ============== MOVEMENT ====================
void AChi::MoveHorizontal(float Value) {
	if (Controller != nullptr && Value != 0.0f) {
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
	
}

void AChi::MoveVertical(float Value) {
	if (Controller != nullptr && Value != 0.0f) {
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AChi::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * 35.0f * GetWorld()->GetDeltaSeconds());
}

void AChi::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	//AddControllerPitchInput(Rate * 33.0f * GetWorld()->GetDeltaSeconds());
	CatapultPitchValue += (Rate * -5.0f);
	if (CatapultPitchValue < 0) CatapultPitchValue = 0;
	else if (CatapultPitchValue > 100) CatapultPitchValue = 100;
}

// ========== SET SPAWN AND LOCATION =================
void AChi::SetWalkSpeed() {
	if (PlayerRef == nullptr) return;
	FVector ChiLocation = GetActorLocation();
	FVector PlayerLocation = PlayerRef->GetActorLocation();

	float distance = FVector::Distance(ChiLocation, PlayerLocation);
	
	// check distance from mokosh
	if (distance > 1000) SpeedMult = 1.3;
	else if (distance < 500) SpeedMult = .3;
	else SpeedMult = 1;

	// set speed with multiplier
	GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed * SpeedMult;
}


// directly spawns chi behind player
void AChi::SpawnBehindPlayer(FVector playerPos) {
	FVector behind = PlayerRef->GetActorForwardVector() * 250.0f;
	playerPos -= behind;
	//playerPos.X += 45;
	playerPos.Z += 300;

	// Move chi to be behind player
	SetActorLocation(playerPos);
}

// sets destination target for behavior tree
void AChi::SetTargetLoc(AActor* player) {
	FVector playerPos = player->GetActorLocation();
	FVector playerForward = player->GetActorForwardVector();
	FVector playerRight = player->GetActorRightVector();

	FVector newPos = playerPos - (playerForward * 140.0f) - (playerRight * 150.0f);

	// set target loc in aicontrollers
	ChiAIControllerRef->UpdateTargetLoc(newPos);
}

void AChi::ActivateCatapult()
{
	//if (IsRespawning) return;
	
	if (AnimInstance->bIsCatapultEnd) return;
	PlayerRef->ShowCatapultSpline = false;
	ToggleAimPivot(false);
	// stop players from spamming launch button

	// AUDIO WHISTLE_THROW
	PlayerRef->PlayWhistle(WhistleType::Throw);
	
	AnimInstance->bIsCatapultEnd = true;
	GetWorld()->GetTimerManager().SetTimer(CatapultTimer, this, &AChi::PlayerLaunched, 0.4f);
}


// ======== CATAPULT =============
// increases walk speed and changes state
void AChi::InitCatapult() {
	ToggleOccludeMaterial(1.0f);
	ChiAIControllerRef->ChangeChiState(ChiStates::Catapult);
	CheckSpeed = false;
	GetCharacterMovement()->MaxWalkSpeed = CatapultWalkSpeed;
}

// called when player is launched to notify chi to change into staff form
void AChi::PlayerLaunched()
{
	// reset animation variables
	AnimInstance->bIsCatapultStart = false;
	AnimInstance->bIsCatapultIdle = false;
	AnimInstance->bIsCatapultEnd = false;
	CatapultReady = false;
	CheckSpeed = true;
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->UnPossess();
	// repossess ai 
	ChiAIControllerRef->ResetCatapult();
	ChiAIControllerRef->Possess(this);
	ChiAIControllerRef->ChangeChiState(ChiStates::Staff);
	PlayerRef->CatapultComponent->LaunchPlayer();

	CatapultPitchValue = 50;
}

// reset catapult variables when cancelling out of catapult
void AChi::CancelCatapult() {
	CatapultReady = false;
	CheckSpeed = true;
	ChiAIControllerRef->CancelCatapult();
	AnimInstance->bIsCatapultStart = false;
	AnimInstance->bIsCatapultIdle = false;
	AnimInstance->bIsCatapultEnd = false;
}

FVector AChi::GetLoadSocketLocation()
{
	return GetMesh()->GetSocketLocation("LoadSocket");
}

// called when chi cannot reach player during catapult init
void AChi::CatapultFailed()
{
	CancelCatapult();
	PlayerRef->UpdateState(EPlayerStates::Neutral);
}

// ======== SAP THROW HELPERS ===========
// set rotation to face direction of sap aim target
void AChi::AimRotation(FVector lookDirection)
{
	FRotator AimDir = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), lookDirection);
	FRotator CurrDir = GetActorRotation();

	SetActorRotation(FRotator(CurrDir.Pitch, AimDir.Yaw, CurrDir.Roll));
}

void AChi::ChangeState(ChiStates NewState)
{
	ChiAIControllerRef->ChangeChiState(NewState);
}

uint8 AChi::GetChiState()
{
	return (uint8)ChiAIControllerRef->GetState();
}


// ============== STAFF ================
void AChi::ChangeToStaff() {
	// set chi invisible
	GetMesh()->SetVisibility(false);
	ToggleChristmasElements(false);
	// deactivates the collider
	SetActorEnableCollision(false);


	// move particle to chi location, and make visible
	StartPos = GetActorLocation();
	EndPos = PlayerRef->GetActorLocation();

	StartTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());

	if(ChiTransformActor != nullptr)
	{
		ChiTransformActor->SetActorHiddenInGame(false);
		ChiTransformActor->SetActorLocation(GetActorLocation());
	}
	
	IsTransforming = true;
	IsToStaff = true;
	LoadTime = .1f;
}


void AChi::ChangeFromStaff(AActor* player) {

	// move particle to staffmesh location and make visible
	
	FVector playerPos = player->GetActorLocation();
	FVector playerForward = player->GetActorForwardVector();
	FVector newPos;
	if(PlayerRef->IsSpawnForMechanic) newPos = playerPos - (playerForward * 320.0f);
	else newPos = playerPos - (playerForward * 50.0f);
	
	newPos.Z += 100;
	
	SetActorEnableCollision(true);
	SetActorLocation(newPos);

	FRotator AimDir = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), playerPos);
	FRotator CurrDir = GetActorRotation();
	SetActorRotation(FRotator(CurrDir.Pitch, AimDir.Yaw, CurrDir.Roll));

	// move particle to chi location, and make visible
	StartPos = PlayerRef->GetActorLocation();
	EndPos = GetActorLocation();

	StartTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());

	if(ChiTransformActor != nullptr)
	{
		ChiTransformActor->SetActorHiddenInGame(false);
		ChiTransformActor->SetActorLocation(EndPos);
	}

	// change chi to appropriate state
	if(PlayerRef->GetState() == EPlayerStates::SapAiming) ChiAIControllerRef->ChangeChiState(ChiStates::Sap);
	else if(PlayerRef->GetState() == EPlayerStates::CatInit) ChiAIControllerRef->ChangeChiState(ChiStates::Catapult);
	else ChiAIControllerRef->ChangeChiState(ChiStates::Neutral);

	
	IsTransforming = true;
	IsToStaff = false;
	LoadTime = .5f;
}


void AChi::LerpParticle()
{
	float fraction = (UGameplayStatics::GetRealTimeSeconds(GetWorld()) - StartTime) / LoadTime;
	FVector lerpLoc = FMath::Lerp(StartPos, EndPos, fraction);
	if(ChiTransformActor != nullptr) ChiTransformActor->SetActorLocation(lerpLoc);

	if (fraction >= 1.0f)
	{
		if (ChiTransformActor != nullptr) ChiTransformActor->SetActorHiddenInGame(true);
		IsTransforming = false;
		// call appropriate niagara particle function
		if(IsToStaff)
		{
			this->PlayTransformationSound(true);
			//ParticleToStaff();
			return;
		}
		this->PlayTransformationSound(false);
		//ParticleToChi();
		GetMesh()->SetVisibility(true);
		ToggleChristmasElements(true);
	}
}

void AChi::RePossess() {
	ChiAIControllerRef->Possess(this);
}

void AChi::RespawnChi()
{
	//IsRespawning = false;
	// if staff mode / cat launched don't respawn
	if (PlayerRef->IsStaffMode()) return;
	// if cat loaded / kill and reset
	if (PlayerRef->GetState() == EPlayerStates::CatLoaded || PlayerRef->GetState() == EPlayerStates::CatLaunched) {
		//  reset catapult values
		//PlayerRef->CameraBoom->ChangeCamera(ECameraTypes::Death);
		PlayerRef->CatapultComponent->CancelCatapult();
		CancelCatapult();
		PlayerRef->KillAndReset();
		PlayerRef->CameraBoom->ChangeCamera(ECameraTypes::Normal);
		return;
	}
	// Chi falls alone (fall off ledge, sink with pillar)
	if(PlayerRef->GetState() == EPlayerStates::Neutral)
	{
		// mokosh neutral - staff and respawn
		FTimerDelegate TimerDel;
		TimerDel.BindUFunction(PlayerRef, FName("TriggerStaff"), false);
		GetWorld()->GetTimerManager().SetTimer(RespawnTimer,TimerDel, 0.15f, false);
	}
}

void AChi::DrawCatapultSpline()
{
	for (int i = 0; i <= 1; i++)
	{
		FVector StartLocation;
		FVector StartTangent;
		FVector EndLocation;
		FVector EndTangent;
		Spline->GetLocationAndTangentAtSplinePoint(i, StartLocation, StartTangent, ESplineCoordinateSpace::Local);
		Spline->GetLocationAndTangentAtSplinePoint(i + 1, EndLocation, EndTangent, ESplineCoordinateSpace::Local);
		SplineMeshes[i]->SetStartAndEnd(StartLocation, StartTangent, EndLocation, EndTangent);
	}
	
}

void AChi::FindParticleHitLocation(FVector StartLocation)
{
	FVector EndLocation = StartLocation;
	EndLocation.Z -= 2000;

	FHitResult hit;
	FCollisionQueryParams params(TEXT("Particle Trace"), true);

	// if hit surface, move sap aim particle 
	if (GetWorld()->LineTraceSingleByChannel(hit, StartLocation, EndLocation, ECC_WorldStatic, params)) {
		FVector NewLoc = hit.Location + (hit.Normal * 20.0f);
		FRotator FaceDirection = hit.ImpactNormal.Rotation();
		
		if(FaceDirection.Pitch != 90.0f) FaceDirection.Pitch = -1 * FaceDirection.Pitch;
		else FaceDirection.Pitch = 0;
		
		float FixedPitch = FMath::Abs(FaceDirection.Pitch - 90);
		float FixedYaw = FMath::Abs(FaceDirection.Yaw - 90);
		
		LastParticle->SetWorldRotation(FaceDirection);
		LastParticle->SetWorldLocation(NewLoc);
	}
}
