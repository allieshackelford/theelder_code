// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_CheckFollowRules.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UBTT_CheckFollowRules : public UBTTaskNode
{
	GENERATED_BODY()
protected:
	class AChiAIController* AIComp;
public:
	UBTT_CheckFollowRules(const FObjectInitializer & ObjectInitializer);

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) override;
};
