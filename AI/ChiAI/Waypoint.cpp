// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "Waypoint.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Player/Mokosh.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
AWaypoint::AWaypoint()
{

	DebugCheck = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DebugCheck"));
	RootComponent = DebugCheck;

	
	// set initial waypoint side
	CurrentSide = FWaypointSide::Right;

	// register all waypoint positions and sides
	Positions.Add(FWaypointSide::Left, FVector(-75, -81, 35));
	Positions.Add(FWaypointSide::Right, FVector(-75, 81, 35));
	Positions.Add(FWaypointSide::Behind, FVector(-75, 0, 35));

	bShouldChangePos = false;
	
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWaypoint::BeginPlay()
{
	Super::BeginPlay();

	PlayerRef = Cast<AMokosh>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	
}

// Called every frame
void AWaypoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	if(bShouldChangePos)
	{
		LerpToPos();
	}
}

void AWaypoint::CalculateWaypointPos(FVector CameraPos)
{
	FVector RightVec = PlayerRef->GetActorRightVector();
	FVector CamToPlayer = PlayerRef->GetActorLocation() - CameraPos;
	float DotProd = FVector::DotProduct(CamToPlayer, RightVec);
	// get angle between vectors
	float Angle = FMath::RadiansToDegrees(acosf(DotProd / (CamToPlayer.Size() * RightVec.Size())));

	// on the right
	if(Angle > 90)
	{
		if(CurrentSide != FWaypointSide::Left)
		{
			CurrentSide = FWaypointSide::Left;
			startPos = PlayerRef->GetFollowRelativeLocation();
			endPos = Positions.FindRef(FWaypointSide::Left);
			//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, TEXT("Waypoint on LEFT"));
			PlayerRef->SetFollowRelativeLocation(endPos);
			//bShouldChangePos = true;
		}
	}
	// angle < 90
	else
	{
		if(CurrentSide != FWaypointSide::Right)
		{
			CurrentSide = FWaypointSide::Right;
			startPos = PlayerRef->GetFollowRelativeLocation();
			endPos = Positions.FindRef(FWaypointSide::Right);
			//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, TEXT("Waypoint on RIGHT"));
			PlayerRef->SetFollowRelativeLocation(endPos);
			//bShouldChangePos = true;
		}
	}
	
}

void AWaypoint::LerpToPos()
{
	float fraction = (UGameplayStatics::GetRealTimeSeconds(GetWorld()) - startTime) / lerpTime;
	FVector lerpPos = FMath::Lerp(startPos, endPos, fraction);
	PlayerRef->SetFollowRelativeLocation(lerpPos);

	if (fraction >= 1.0f)
	{
		fraction = 0;
		bShouldChangePos = false;
	}
}

