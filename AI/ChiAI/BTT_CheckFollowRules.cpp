// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "BTT_CheckFollowRules.h"
#include "AI/ChiAI/ChiAIController.h"

UBTT_CheckFollowRules::UBTT_CheckFollowRules(const FObjectInitializer& ObjectInitializer)
{
}

EBTNodeResult::Type UBTT_CheckFollowRules::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	// perform checks for chi
	if (AIComp == nullptr)
	{
		AIComp = Cast<AChiAIController>(OwnerComp.GetAIOwner());
	}

	if (AIComp->CheckChiBehindPlayer() == false || AIComp->CheckWaypointDistance() == false) return EBTNodeResult::Failed;

	return EBTNodeResult::Succeeded;
}
