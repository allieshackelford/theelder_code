// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/BaseAI/BaseAIController.h"
#include "AI/ChiAI/Chi.h"
#include "Containers/Queue.h"
#include "Player/Mokosh.h"
#include "ChiAIController.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class ChiStates : uint8 {
	Neutral UMETA(DisplayName = "Neutral"),
	Catapult UMETA(DisplayName = "Catapult"),
	Staff UMETA(DisplayName = "Staff"),
	Heal UMETA(DisplayName = "Heal"),
	Sap UMETA(DisplayName = "Sap")
};

UCLASS()
class THEELDER_API AChiAIController : public ABaseAIController
{
	GENERATED_BODY()

protected:
	class AMokosh* PlayerRef;
	ChiStates State;
	class AChi* ChiRef;

public:
	AChiAIController(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void OnPossess(APawn* InPawn) override;

	void ResetBT();

	void ChangeChiState(ChiStates newState);
	ChiStates GetState();
	
	void UpdateTargetLoc(FVector newLocation);

	void ResetCatapult();
	void CancelCatapult();


	UFUNCTION(BlueprintCallable)
	bool CheckChiBehindPlayer();

	UFUNCTION(BlueprintCallable)
	bool CheckWaypointDistance();
};