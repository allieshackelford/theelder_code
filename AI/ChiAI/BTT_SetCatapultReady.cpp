// Fill out your copyright notice in the Description page of Project Settings.


#include "BTT_SetCatapultReady.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "AI/ChiAI/Chi.h"
#include "Runtime/AIModule/Classes/AIController.h"

EBTNodeResult::Type UBTT_SetCatapultReady::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) {
	OwnerComp.GetBlackboardComponent()->SetValueAsBool("CatapultReady",true);

	//AChiAIController* Controller = Cast<AChiAIController>(OwnerComp.GetAIOwner());
	AChi* ChiRef = Cast<AChi>(OwnerComp.GetAIOwner()->GetPawn());

	if (ChiRef != nullptr) {
		ChiRef->CatapultReady = true;
	}


	return EBTNodeResult::Succeeded;
}