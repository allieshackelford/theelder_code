// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BaseAIController.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API ABaseAIController : public AAIController
{
	GENERATED_BODY()
protected: 

	// ===== BASE AI COMPONENTS ======
	UPROPERTY(EditInstanceOnly)
	class UBehaviorTreeComponent* BehaviorComponent;

	UPROPERTY(EditInstanceOnly)
	class UBlackboardComponent* BlackboardComponent;

	UPROPERTY(EditDefaultsOnly, Category = "AI")
	class UBehaviorTree* BehaviorTree;

protected:

	ABaseAIController(const FObjectInitializer& ObjectInitializer);

	virtual void OnPossess(APawn* InPawn) override;

public:
	UBlackboardComponent* GetBlackboardComponent();
};
