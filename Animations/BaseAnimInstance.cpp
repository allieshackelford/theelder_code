// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "BaseAnimInstance.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GameFramework/Character.h"
#include "Engine/Engine.h"

void UBaseAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	AnimOwner = Cast<ACharacter>(TryGetPawnOwner());
}

void UBaseAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	// Check for pawn or reinitialize
	if (AnimOwner == nullptr)
	{
		NativeInitializeAnimation();
		return;
	}

	// Get Character Info
	FRotator CharacterRotation = AnimOwner->GetActorRotation();
	FVector CharacterVelocity = AnimOwner->GetVelocity();
	bool IsCharacterFalling = AnimOwner->GetMovementComponent()->IsFalling();
	

	// Set Instance Variables
	Direction = CalculateDirection(CharacterVelocity, CharacterRotation);
	Speed = CharacterVelocity.Size();
	bIsFalling = IsCharacterFalling;
}
