// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "BaseAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UBaseAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		bool bIsFalling = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		float Direction;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		float Speed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		bool bIsAlive = true;


protected:
	class ACharacter* AnimOwner;
	
protected:
	virtual void NativeInitializeAnimation() override;

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
};
