// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "ChiAnimInstance.h"
#include "GameFramework/PawnMovementComponent.h"
#include "AI/ChiAI/Chi.h"
#include "Engine/Engine.h"

void UChiAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
	ChiOwner = Cast<AChi>(TryGetPawnOwner());
}

void UChiAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	// Check for pawn or reinitialize
	if (ChiOwner == nullptr)
	{
		NativeInitializeAnimation();
		
		return;
	}
	
	
	//FRotator CharacterAimDirection = AnimOwner->GetAimDirection();
	//AimPitch = CharacterAimDirection.Pitch

}
