// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Animations/BaseAnimInstance.h"
#include "Player/Mokosh.h"
#include "MokoshAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UMokoshAnimInstance : public UBaseAnimInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		float JoystickDirectionX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		float JoystickDirectionY;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		bool ClimbJump;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		bool DetachFromClimbing;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		bool MantleUpLedge;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		bool EnvironmentStab;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		float DistanceFromGround;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		float DownwardVelocity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		FVector MokoshVelocity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		bool MudDeath;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		bool CrystalAiming;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		EClimbingDirectionEnum ClimbingDirection;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	EPlayerStates PlayerState;

protected:
	class AMokosh* MokoshOwner;

public:
	UFUNCTION()
		virtual void NativeInitializeAnimation() override;

	UFUNCTION()
		virtual void NativeUpdateAnimation(float DeltaSeconds) override;
};
