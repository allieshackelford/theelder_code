// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "ChiIndicator.h"
#include "UMG/Public/Components/Image.h"
#include "UMG/Public/Components/CanvasPanelSlot.h"
#include "UMG/Public/Components/Overlay.h"
#include "Engine.h"
#include "Engine/UserInterfaceSettings.h"
#include "TheElderGameMode.h"
#include "AI/ChiAI/Chi.h"

bool UChiIndicator::Initialize()
{
	bool bResult = Super::Initialize();

	// Check for base class
	if (!bResult)
	{
		return false;
	}

	return true;
}


void UChiIndicator::NativeConstruct()
{
	Super::NativeConstruct();

	IsStaff = false;

	RingImage->SetVisibility(ESlateVisibility::Hidden);
	NegativeChiImage->SetVisibility(ESlateVisibility::Hidden);
	StaffImage->SetVisibility(ESlateVisibility::Hidden);
}

void UChiIndicator::ToggleChiIndicator(bool IsPositive)
{
	RingImage->SetVisibility(ESlateVisibility::Visible);
	NegativeChiImage->SetVisibility(ESlateVisibility::Visible);
	if(IsStaff)
	{
		StaffImage->SetVisibility(ESlateVisibility::Hidden);
		GetWorld()->GetTimerManager().SetTimer(ResetIndicatorTimer, this, &UChiIndicator::ToggleStaffIndicator, 1.0f);
	}
	else GetWorld()->GetTimerManager().SetTimer(ResetIndicatorTimer, this, &UChiIndicator::ResetIndicator, 2.0f);
}

void UChiIndicator::ToggleStaffIndicator()
{
	IsStaff = true;
	RingImage->SetVisibility(ESlateVisibility::Visible);
	StaffImage->SetVisibility(ESlateVisibility::Visible);

	NegativeChiImage->SetVisibility(ESlateVisibility::Hidden);
}

void UChiIndicator::ResetIndicator()
{
	IsStaff = false;
	RingImage->SetVisibility(ESlateVisibility::Hidden);
	NegativeChiImage->SetVisibility(ESlateVisibility::Hidden);
	StaffImage->SetVisibility(ESlateVisibility::Hidden);
}
