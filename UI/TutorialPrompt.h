// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TutorialPrompt.generated.h"

UENUM(BlueprintType)
enum class EInputTypes: uint8{
	Catapult UMETA(DisplayName="Catapult"),
	Interact UMETA(DisplayName="Interact"),
	Jump UMETA(DisplayName="Jump"),
	Drop UMETA(DisplayName="Drop"),
	Sap UMETA(DisplayName="Sap")
};


UCLASS()
class THEELDER_API ATutorialPrompt : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATutorialPrompt();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CustomPrompt")
		TSubclassOf<class UUserWidget> wPromptToShow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CustomPrompt")
		float SecondsToShowPrompt;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CustomPrompt")
		float SecondsBeforeShowingAgain;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CustomPrompt")
		int TimesToPressBeforeNotShowing;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CustomPrompt")
		EInputTypes PromptType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CustomPrompt (DO NOT CHANGE)")
		int TimesInputPressed;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CustomPrompt (DO NOT CHANGE)")
		bool bShouldShowPrompt;

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintCallable)
		void IncrementInputCount();

	UFUNCTION(BlueprintCallable)
		void ShowPrompt(bool ToShow);


	UFUNCTION(BlueprintImplementableEvent)
		void TimerPromptEvent();
	
};
