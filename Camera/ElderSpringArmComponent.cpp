// Fill out your copyright notice in the Description page of Project Settings.


#include "ElderSpringArmComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Player/Mokosh.h"
#include "Engine/Engine.h"
#include "CollisionQueryParams.h"
#include "AI/ChiAI/Chi.h"
#include "Kismet/KismetMathLibrary.h"
#include "Camera/CameraComponent.h"

UElderSpringArmComponent::UElderSpringArmComponent() {
	TargetArmLength = 600;
	bUsePawnControlRotation = true; // Rotate the arm based on the controller
	bEnableCameraLag = true;
	CameraLagSpeed = 20;
	CameraLagMaxDistance = 10;
	ProbeChannel = ECC_Camera;
	
	ChangeToView = false; 
	ChangeRot = false;

	CurrentCam = ECameraTypes::Normal;

	CameraChanges.Add(ECameraTypes::Normal, FCameraChangeParams(600, FVector(0, 0, 100), FRotator(0, 0, 0), false, .3f));
	CameraChanges.Add(ECameraTypes::Climbing, FCameraChangeParams(850, FVector(0, 0, 250), FRotator(-20, 0, 0), true, .3f));
	CameraChanges.Add(ECameraTypes::SapAim, FCameraChangeParams(250, FVector(0, 0, 150), FRotator(0, 0, 0), false, .2f));
	CameraChanges.Add(ECameraTypes::Cinematic, FCameraChangeParams(350, FVector(0, 0, 100), FRotator(0, 100.0f, 0), true, .6f));
	CameraChanges.Add(ECameraTypes::Respawn, FCameraChangeParams(600, FVector(0, 0, 100), FRotator(0, 90.0f, 0), true, .3f));
	CameraChanges.Add(ECameraTypes::Death, FCameraChangeParams(900, FVector(0, 0, 550), FRotator(0, 0, 0), false, 0.8f));
	CameraChanges.Add(ECameraTypes::BossSmash, FCameraChangeParams(1750, FVector(0, 0, 100), FRotator(0, 0, 0), false, 1.5f));
	
	
	PrimaryComponentTick.bCanEverTick = true;
}

void UElderSpringArmComponent::BeginPlay() {
	Super::BeginPlay();

	Ref = Cast<AMokosh>(GetOwner());
	Controller = UGameplayStatics::GetPlayerController(GetWorld(), 0);
}

void UElderSpringArmComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (Ref == nullptr) return;
	// hide chi if in front of camera
	if(Ref->GetState() == EPlayerStates::Neutral ||
		Ref->GetState() == EPlayerStates::SapAiming) HideChi();
	
	// check for climbing lerp 
	if (ChangeToView) {
		LerpToView();
	}
}


void UElderSpringArmComponent::UpdateCameraPitchCurve(float currPitch) {
	// update the camera based on pitch change
	if (currPitch < -24) {
		//TargetArmLength = Remap(currPitch, FVector2D(-24, -89), FVector2D(800, 1000));
		TargetArmLength = Remap(currPitch, FVector2D(-24, -75), FVector2D(550, 1000));
	}
	else{
		//TargetArmLength = Remap(currPitch, FVector2D(43, -24), FVector2D(500, 800));
		TargetArmLength = Remap(currPitch, FVector2D(43, -24), FVector2D(160, 550));
	}
}


// call in mokosh.cpp when camera is aiming
void UElderSpringArmComponent::UpdateAimingPitchCurve(float currPitch)
{
	TargetArmLength = Remap(currPitch, FVector2D(70, -89), FVector2D(200, 375));
}

float UElderSpringArmComponent::Remap(float val, FVector2D xy1, FVector2D xy2) {
	return (val - xy1.X) / (xy1.Y - xy1.X) * (xy2.Y - xy2.X) + xy2.X;
}

void UElderSpringArmComponent::ChangeCamera(ECameraTypes cameraType) {
	ECameraTypes prevCam = CurrentCam;
	CurrentCam = cameraType;

	FCameraChangeParams params = CameraChanges.FindRef(cameraType);

	startTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	startLength = TargetArmLength;
	endLength = params.ArmLength;
	startOffset = TargetOffset;
	endOffset = params.Offset;
	startRot = GetRelativeTransform().GetRotation().Rotator();
	startControlRot = Controller->GetControlRotation();
	endRot = params.RelRotation;
	endControlRot = Ref->GetActorForwardVector().Rotation();
	ChangeRot = params.ChangeRotation;
	loadTime = params.LoadTime;

	// remap normal camera correctly
	if(CurrentCam == ECameraTypes::Normal)
	{
		bDoCollisionTest = true;
		FRotator rotation = Ref->CamManager->GetCameraRotation();
		if (rotation.Pitch < -24) endLength = Remap(rotation.Pitch, FVector2D(-24, -75), FVector2D(550, 1000));

		else endLength = Remap(rotation.Pitch, FVector2D(43, -24), FVector2D(160, 550));
		
	}
	if (CurrentCam == ECameraTypes::Cinematic) endLength = TargetArmLength;
	if (CurrentCam == ECameraTypes::Normal && prevCam == ECameraTypes::Cinematic) {
		bUsePawnControlRotation = true;
		CameraRotationLagSpeed = 20.0f;
		loadTime = 10.0f;
		Controller->SetControlRotation(Ref->GetActorRotation());
	}

	if (CurrentCam == ECameraTypes::Normal && prevCam == ECameraTypes::Climbing) {
		//bUsePawnControlRotation = true;
		bEnableCameraRotationLag = false;
		ChangeRot = true;
		loadTime = 1.2f;
		Controller->SetControlRotation(Ref->GetActorRotation());
	}

	if (CurrentCam == ECameraTypes::Climbing ||
		CurrentCam == ECameraTypes::Cinematic) {
		bDoCollisionTest = false;
		bUsePawnControlRotation = false;
		bEnableCameraRotationLag = true;
		CameraRotationLagSpeed = 1.2f;
	}

	
	// start lerping camera
	ChangeToView = true;

	
	if(CurrentCam == ECameraTypes::Respawn)
	{
		ChangeToView = false;
		ChangeRot = false;
		bUsePawnControlRotation = false;
		SetRelativeRotation(FRotator(0, 0.0f, 0));
		TargetArmLength = 600.0f;
		TargetOffset = FVector(0, 0, 100);
		Ref->FollowCamera->SetRelativeRotation(FRotator(0,-10,0).Quaternion(), false);
		Controller->SetControlRotation(FRotator(0, 0.0f, 0));
		bUsePawnControlRotation = true;
		CurrentCam = ECameraTypes::Normal;
	}
}

void UElderSpringArmComponent::LerpToView() {
	float fraction = (UGameplayStatics::GetRealTimeSeconds(GetWorld()) - startTime) / loadTime;
	float lerpArmLength = FMath::Lerp(startLength, endLength, fraction);
	FVector lerpOffset = FMath::Lerp(startOffset, endOffset, fraction);
	FRotator lerpRot = FMath::Lerp(startRot, endRot, fraction);

	TargetArmLength = lerpArmLength;
	TargetOffset = lerpOffset;

	if (ChangeRot) {
		SetRelativeRotation(lerpRot);
		Controller->SetControlRotation(endControlRot);
	}

	if (fraction >= 1.0f)
	{
		fraction = 0;
		ChangeToView = false;
		ChangeRot = false;

		/*
		 * 
		if(CurrentCam == ECameraTypes::Respawn)
		{
			SetRelativeRotation(FRotator(0, 90.0f, 0));
			Controller->SetControlRotation(FRotator(0, 90.0f, 0));
			CurrentCam = ECameraTypes::Normal;
		}
		 */
		if (CurrentCam == ECameraTypes::Normal) {
			bUsePawnControlRotation = true;
			bEnableCameraRotationLag = false;
		}
	}
}

void UElderSpringArmComponent::HideChi()
{
	if (Ref->GetChiRef() == nullptr) return;

	float CameraToChiDist = FVector::Distance(Ref->GetChiRef()->GetActorLocation(), Ref->CamManager->GetCameraLocation());

	if (CameraToChiDist < 450) {
		Ref->GetChiRef()->ToggleOccludeMaterial(.001f);
	}
	else Ref->GetChiRef()->ToggleOccludeMaterial(1.0f);
}

// call every .5 seconds
void UElderSpringArmComponent::CheckToHideObjects()
{
	FHitResult Hit;
	FCollisionQueryParams params(TEXT("HideObjects"), true);
	// ray cast from camera location to player location
	if (Ref == nullptr || Ref->CamManager == nullptr) return;
	FVector Start = Ref->CamManager->GetCameraLocation();
	FVector End = Ref->GetActorLocation();

	// check for ray cast from camera to player
	if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, params))
	{
		AMokosh* CheckPlayer = Cast<AMokosh>(Hit.Actor);
		if(CheckPlayer == nullptr)
		{
			// add hit actor to hidden
			AActor* hideMe = Hit.GetActor();
			hideMe->SetActorHiddenInGame(true);
			HiddenActors.Add(hideMe);
		}
	}

}


// call when rotating
void UElderSpringArmComponent::UnHideObjects()
{
	for(int i = 0; i < HiddenActors.Num(); i++)
	{
		AActor* unhideMe = HiddenActors[i];
		unhideMe->SetActorHiddenInGame(false);
		HiddenActors.Remove(unhideMe);
	}
}

void UElderSpringArmComponent::CinematicLookAtTarget(FVector TargetLoc)
{
	bUsePawnControlRotation = false;
	FRotator LookRotation = UKismetMathLibrary::FindLookAtRotation(Ref->CamManager->GetCameraLocation(), TargetLoc);
	//Ref->CamManager->SetActorRotation(LookRotation);
	SetWorldRotation(LookRotation);
	Controller->SetControlRotation(endControlRot);
	bUsePawnControlRotation = true;
}

void UElderSpringArmComponent::ResetCamera()
{
	Ref->FollowCamera->SetRelativeRotation(FRotator(0,0,0));
	
	ChangeCamera(ECameraTypes::Normal);
	Ref->UpdateState(EPlayerStates::Neutral);
}
