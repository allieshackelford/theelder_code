// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShake.h"
#include "ElderCameraShake.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UElderCameraShake : public UCameraShake
{
	GENERATED_BODY()
public:
	UElderCameraShake();
};
