// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "HttpService.h"
#include <stdio.h>
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Player/Mokosh.h"
#include "Engine/World.h"

// Sets default values
AHttpService::AHttpService()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}


// Called when the game starts or when spawned
void AHttpService::BeginPlay()
{
	Super::BeginPlay();

	Http = &FHttpModule::Get();

	playerRef = Cast<AMokosh>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));

}

TSharedRef<IHttpRequest> AHttpService::RequestWithRoute(FString Subroute) {
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->SetURL(ApiBaseUrl + Subroute);
	SetRequestHeaders(Request);
	return Request;
}

void AHttpService::SetRequestHeaders(TSharedRef<IHttpRequest>& Request) {
	Request->SetHeader(TEXT("User-Agent"), TEXT("X-UnrealEngine-Agent"));
	Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
	Request->SetHeader(TEXT("Accepts"), TEXT("application/json"));
}

TSharedRef<IHttpRequest> AHttpService::GetRequest(FString Subroute) {
	TSharedRef<IHttpRequest> Request = RequestWithRoute(Subroute);
	Request->SetVerb("GET");
	return Request;
}

TSharedRef<IHttpRequest> AHttpService::PostRequest(FString Subroute, FString ContentJsonString) {
	TSharedRef<IHttpRequest> Request = RequestWithRoute(Subroute);
	Request->SetVerb("POST");
	Request->SetContentAsString(ContentJsonString);
	return Request;
}

void AHttpService::Send(TSharedRef<IHttpRequest>& Request) {
	Request->ProcessRequest();
}

bool AHttpService::ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful) {
	if (!bWasSuccessful || !Response.IsValid()) return false;
	if (EHttpResponseCodes::IsOk(Response->GetResponseCode())) return true;
	else {
		UE_LOG(LogTemp, Warning, TEXT("Http Response returned error code: %d"), Response->GetResponseCode());
		return false;
	}
}

void AHttpService::SetAuthorizationHash(FString Hash, TSharedRef<IHttpRequest>& Request) {
	Request->SetHeader(AuthorizationHeader, Hash);
}



/**************************************************************************************************************************/



template <typename StructType>
void AHttpService::GetJsonStringFromStruct(StructType FilledStruct, FString& StringOutput) {
	FJsonObjectConverter::UStructToJsonObjectString(StructType::StaticStruct(), &FilledStruct, StringOutput, 0, 0);
}

template <typename StructType>
void AHttpService::GetStructFromJsonString(FHttpResponsePtr Response, StructType& StructOutput) {
	StructType StructData;
	FString JsonString = Response->GetContentAsString();
	FJsonObjectConverter::JsonObjectStringToUStruct<StructType>(JsonString, &StructOutput, 0, 0);
}



/**************************************************************************************************************************/


// ================ PLAYER POSITION ================
void AHttpService::SendPosition(FRequest_PlayerPos PlayerPos) {
	FString ContentJsonString;
	GetJsonStringFromStruct<FRequest_PlayerPos>(PlayerPos, ContentJsonString);
	TSharedRef<IHttpRequest> Request = PostRequest("admin", ContentJsonString);
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpService::PositionResponse);
	Send(Request);
}

void AHttpService::PositionResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {
	if (!ResponseIsValid(Response, bWasSuccessful)) return;

	FResponse_PlayerPos PositionResponse;
	GetStructFromJsonString<FResponse_PlayerPos>(Response, PositionResponse);

	UE_LOG(LogTemp, Warning, TEXT("Id is: %d"), PositionResponse.id);
}


// ================ CHI STATE ================

void AHttpService::SendChiState(FRequest_ChiState State) {
	FString ContentJsonString;
	GetJsonStringFromStruct<FRequest_ChiState>(State, ContentJsonString);
	TSharedRef<IHttpRequest> Request = PostRequest("admin", ContentJsonString);
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpService::StateResponse);
	Send(Request);
}


void AHttpService::StateResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {
	if (!ResponseIsValid(Response, bWasSuccessful)) return;

	FResponse_ChiState StateResponse;
	GetStructFromJsonString<FResponse_ChiState>(Response, StateResponse);

	UE_LOG(LogTemp, Warning, TEXT("Id is: %d"), StateResponse.id);
}

// ================ REGISTER AREA ================

void AHttpService::RegisterArea(FRequest_Area NewArea) {
	FString ContentJsonString;
	GetJsonStringFromStruct<FRequest_Area>(NewArea, ContentJsonString);
	TSharedRef<IHttpRequest> Request = PostRequest("admin", ContentJsonString);
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpService::AreaResponse);
	Send(Request);
}

void AHttpService::AreaResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {
	if (!ResponseIsValid(Response, bWasSuccessful)) return;

	FResponse_Area AreaResponse;
	GetStructFromJsonString<FResponse_Area>(Response, AreaResponse);

	UE_LOG(LogTemp, Warning, TEXT("Id is: %d"), AreaResponse.id);
}

// =================== QA SESSION ===================

void AHttpService::SendQASession(FRequest_QASession NewQA) {
	FString ContentJsonString;
	GetJsonStringFromStruct<FRequest_QASession>(NewQA, ContentJsonString);
	TSharedRef<IHttpRequest> Request = PostRequest("admin", ContentJsonString);
	Request->OnProcessRequestComplete().BindUObject(this, &AHttpService::QASessionResponse);
	Send(Request);
}

void AHttpService::QASessionResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {
	if (!ResponseIsValid(Response, bWasSuccessful)) return;

	FResponse_QASession QAResponse;
	GetStructFromJsonString<FResponse_QASession>(Response, QAResponse);

	UE_LOG(LogTemp, Warning, TEXT("Id is: %d"), QAResponse.id);
}