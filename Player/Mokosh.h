// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ElderCharacter.h"
#include "ClimbingComponent.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Camera/ElderSpringArmComponent.h"
#include "Mokosh.generated.h"


struct FPredictProjectilePathParams;
struct FPredictProjectilePathResult;

UENUM(BlueprintType)
enum class ECustomMovementModesEnum : uint8
{
	MME_Climbing 	UMETA(DisplayName = "Climbing"),
};

UENUM(BlueprintType)
enum class EPlayerStates : uint8 {
	Neutral UMETA(DisplayName = "Neutral"), 
	CatInit UMETA(DisplayName = "CatInit"), 
	CatLoading UMETA(DisplayName = "CatLoading"),
	CatLoaded UMETA(DisplayName = "CatLoaded"), 
	CatLaunched UMETA(DisplayName = "CatLaunched"), 
	FallSave UMETA(DisplayName = "FallSave"), 
	Climbing UMETA(DisplayName = "Climbing"),
	
	SapAiming UMETA(DisplayName = "SapAiming"), 
	Bouncing UMETA(DisplayName = "Bouncing"), 
	/* A special event that acts like a jump but the player can't move*/
	BossThrown UMETA(DisplayName = "BossThrown"),
	/* Mantling is a special little snowflake because we need to be in flying movement mode, and not be able to move during the mantle*/
	Mantling UMETA(DisplayName = "Mantling"),
	/* Cinematic mode so player can't do any inputs*/
	Cinematic UMETA(DisplayName = "Cinematic")
};


UENUM(BlueprintType)
enum class WhistleType : uint8 {
	CallCatapult UMETA(DisplayName = "CallCatapult"),
	Shoot UMETA(DisplayName = "Shoot"),
	Throw UMETA(DisplayName = "Throw")
};
/**
 * 
 */
UCLASS()
class THEELDER_API AMokosh : public AElderCharacter
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* StaffMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USkeletalMeshComponent* WingedStaffMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UElderSpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UCameraComponent* FollowCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UClimbingComponent* ClimbingComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UCatapultComponent* CatapultComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USapThrowComponent* SapThrowComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<AActor> ClassToFind;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UChildActorComponent* ChiFollowPoint;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USceneComponent* StaffCrystalSpawnPoint;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraSpeed")
		float TurnRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraSpeed")
		float LookUpRate;

	// to update in vine trigger blueprint
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsStabbing;

	UPROPERTY()
		FTimerHandle PossessTimer;

	UPROPERTY()
		FTimerHandle BounceTimer;

	UPROPERTY()
		FTimerHandle ResetTimer;

	UPROPERTY()
		FTimerHandle CinematicTimer;

	
	// ========= SAP VIEW COMPONENTS ==================
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UChildActorComponent* SapCamActor;
	
public:
	bool CheckDistance;
	bool CheckForChiSpawn;
	bool doneClimbing;

	bool staffMode;
	bool ChiCam;

	bool bWillFallSave;
	float prevFallDist;

	bool ShowCatapultSpline;

	bool IsSpawnForMechanic;
	
	// ====== ANIMATION VARIABLES ========
	float InputDirectionX;
	float InputDirectionY;
	bool IsClimbJumping;
	bool IsDetachClimbing;
	bool IsMantleLedge;
	bool IsAiming;
	EClimbingDirectionEnum ClimbingDirection;

	class AElderPlayerController* MokoshPlayerController;
	class APlayerCameraManager* CamManager;
	

protected:
	float prevPitch;
	
	class AChi* ChiRef;
	EPlayerStates State = EPlayerStates::Neutral;
	bool wasClimbing;


	class AWaypoint* WaypointRef;
protected:
	AMokosh();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
public:

	AChi* GetChiRef();
	
	// ====== STATE GET/UPDATE =========
	UFUNCTION(BlueprintCallable)
	EPlayerStates GetState();

	UFUNCTION(BlueprintCallable)
	void UpdateState(EPlayerStates newState);

	UFUNCTION(BlueprintCallable)
	bool IsStaffMode();

	void SummonChi();

	// ========== STATE CHANGE TESTS =========
	float CheckChiDistance();
	bool TeleportChi();

	void SetCollisionToIgnore();
	void ResetCollisions();

	FVector GetFollowRelativeLocation();
	void SetFollowRelativeLocation(FVector lerpLoc);

	// ========= MOVEMENT ============
	void MoveHorizontal(float Value);
	void MoveVertical(float Value);
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);

	void RotateToFaceCamera();

	// ========= INPUT ACTIONS ==========
	void Jump() override;
	void CancelPressed();
	void Interact();

	// ======== STAFF =============
	UFUNCTION(BlueprintCallable)
	void TriggerStaff(bool ShouldBeWings);
	UFUNCTION(BlueprintCallable)
	void TriggerChi();
	
	bool CheckRespawnChi();

	bool TriggerChiOutOfCamera();

	// ======== BOUNCING =============
	UFUNCTION(BlueprintCallable)
	bool CheckBounceStart();
	void CheckBounceStop();

	// ======= FALL SAVE ========
	UFUNCTION(BlueprintCallable)
	float CheckFloorDist();
	void CheckShouldFallSave();
	void ActivateFallSave();

	// ===== CANCEL STATE =======
	void CancelSapAim();
	ECameraTypes GetCameraType();

	// ===== DAMAGE/RESPAWN =======
	void KillAndReset();
	void ResetLocation();

	UFUNCTION(BlueprintImplementableEvent)
		void PlayWingStaffMontage();

	UFUNCTION(BlueprintImplementableEvent)
		void ChangeStaffMaterial(bool IsChi);

	UFUNCTION(BlueprintImplementableEvent)
		void PlayWhistle(WhistleType Whistle);

	UFUNCTION(BlueprintImplementableEvent)
		bool IsChiNavigable(FVector Start, FVector End);
	
	UFUNCTION()
	/* Binded from the OnTakeAnyDamage event*/
	void OnTakenAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
		AController* InstigatedBy, AActor* DamageCauser);
	UFUNCTION()
	/* Binded from the OnTakePointDamage event*/
	void OnTakenPointDamage(AActor* DamagedActor, float Damage, AController* InstigatedBy, FVector HitLocation,
		UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const UDamageType* DamageType, AActor* DamageCauser);

	// ===== TRIGGER STAB ======
	UFUNCTION(BlueprintCallable)
		void StabCamera(FVector TargetLoc);

	
};
