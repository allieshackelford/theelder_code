// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "UI/CrystalCrosshair.h"
#include "SapThrowComponent.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class THEELDER_API USapThrowComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USapThrowComponent();

	// ========= SAP VIEW COMPONENTS ==================
	UPROPERTY()
	FTimerHandle SapTimer;

	UPROPERTY()
	FTimerHandle AnimationTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AActor> BPSap;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int RapidFireCoolDown;

protected:
	class AMokosh* MOwner;
	class AElderPlayerController* PlayerControllerRef;

	int InputHoldTime;
	bool StartInputTimer;

	// ====== CAMERA HIDDEN OBJECTS ======
	TArray<AActor*> HiddenFromCamera;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:	

	// ======= CHECK SYSTEM ==========
	bool CheckIfCanActivate();

	// =========== SAP THROW ==============================
	void EnterSapAim();
	void Aiming();
	void ExitSapAim();
	void SapThrow();
	void SapThrowInputReleased();
	void ToggleSap();

	void ResetThrowAnimation();

	bool TargetBehindPlayer(FVector HitLocation);
};
