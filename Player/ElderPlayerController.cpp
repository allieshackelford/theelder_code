// Fill out your copyright notice in the Description page of Project Settings.


#include "ElderPlayerController.h"
#include "UI/CrystalCrosshair.h"
#include "UI/ChiIndicator.h"
#include "Components/Image.h"
#include "Engine/Engine.h"

AElderPlayerController::AElderPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}


void AElderPlayerController::BeginPlay() {
	Super::BeginPlay();

	if (wMainMenu) {
		mMainMenu = CreateWidget<UUserWidget>(this, wMainMenu);
	}

	if (wOptionsMenu) {
		mOptionsMenu = CreateWidget<UUserWidget>(this, wOptionsMenu);
	}

	if (wPauseMenu) {
		mPauseMenu = CreateWidget<UUserWidget>(this, wPauseMenu);
	}

	if(wReticle)
	{
		mReticle = CreateWidget<UCrystalCrosshair>(this, wReticle);
		if (mReticle) {
			mReticle->AddToViewport();
			mReticle->SetVisibility(ESlateVisibility::Hidden);
		}
	}

	if(wChiIndicator)
	{
		mChiIndicator = CreateWidget<UChiIndicator>(this, wChiIndicator);
		if(mChiIndicator)
		{
			mChiIndicator->AddToViewport();
		}
	}
}

void AElderPlayerController::ToggleReticleOn()
{
	mReticle->SetVisibility(ESlateVisibility::Visible);
}

void AElderPlayerController::ToggleReticleOff()
{
	mReticle->SetVisibility(ESlateVisibility::Hidden);
}

void AElderPlayerController::ToggleReticleHittable(bool IsHittable)
{
	mReticle->ToggleReticle(IsHittable);
}

void AElderPlayerController::TriggerChiIndicator()
{
	mChiIndicator->ToggleChiIndicator(false);
}


/*
 * 
void AElderPlayerController::UpdateRotation(float DeltaTime)
{
	// Calculate Delta to be applied on ViewRotation
	FRotator DeltaRot(RotationInput * DeltaTime);

	FRotator ViewRotation = GetControlRotation();

	if (PlayerCameraManager)
	{
		PlayerCameraManager->ProcessViewRotation(DeltaTime, ViewRotation, DeltaRot);
	}

	AActor* ViewTarget = GetViewTarget();
	if (!PlayerCameraManager || !ViewTarget || !ViewTarget->HasActiveCameraComponent() || ViewTarget->HasActivePawnControlCameraComponent())
	{
		if (IsLocalPlayerController() && GEngine->XRSystem.IsValid() && GEngine->XRSystem->IsHeadTrackingAllowed())
		{
			auto XRCamera = GEngine->XRSystem->GetXRCamera();
			if (XRCamera.IsValid())
			{
				XRCamera->ApplyHMDRotation(this, ViewRotation);
			}
		}
	}

	SetControlRotation(ViewRotation);

	APawn* const P = GetPawnOrSpectator();
	if (P)
	{
		P->FaceRotation(ViewRotation, DeltaTime);
	}
}
*/
