// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Json.h"
#include "JsonUtilities.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "HttpService.generated.h"

// ==== Put in another file later ========
// **** PLAYER POSITION *********
USTRUCT()
struct FRequest_PlayerPos {
	GENERATED_BODY()
	UPROPERTY(EditAnywhere) FString cmd;
	UPROPERTY(EditAnywhere) FString playSession;
	UPROPERTY(EditAnywhere) float posX;
	UPROPERTY(EditAnywhere) float posY;

	FRequest_PlayerPos() {}
};

USTRUCT()
struct FResponse_PlayerPos {
	GENERATED_BODY()
	UPROPERTY(EditAnywhere) int id;

	FResponse_PlayerPos() {}
};

// ***** CHI STATE *******
USTRUCT()
struct FRequest_ChiState {
	GENERATED_BODY()
	UPROPERTY(EditAnywhere) FString cmd;
	UPROPERTY(EditAnywhere) FString playSession;
	UPROPERTY(EditAnywhere) int state; // CHISTATES ENUM

	FRequest_ChiState() {}
};

USTRUCT()
struct FResponse_ChiState {
	GENERATED_BODY()
	UPROPERTY(EditAnywhere) int id;

	FResponse_ChiState() {}
};

// **** AREA REGISTER *********
USTRUCT()
struct FRequest_Area {
	GENERATED_BODY()
	UPROPERTY(EditAnywhere) FString cmd;
	UPROPERTY(EditAnywhere) FString name;
	UPROPERTY(EditAnywhere) FVector startPos;
	UPROPERTY(EditAnywhere) FVector endPos;

	FRequest_Area() {}
};


USTRUCT()
struct FResponse_Area {
	GENERATED_BODY()
	UPROPERTY(EditAnywhere) int id;

	FResponse_Area() {}
};

// **** QA Session ******

USTRUCT()
struct FRequest_QASession {
	GENERATED_BODY()
	UPROPERTY(EditAnywhere) FString cmd;
	UPROPERTY(EditAnywhere) FString sessionid;
	UPROPERTY(EditAnywhere) FString area;
	UPROPERTY(EditAnywhere) bool firstTime;

	FRequest_QASession() {}
};

USTRUCT()
struct FResponse_QASession {
	GENERATED_BODY()
	UPROPERTY(EditAnywhere) int id; // **IMPORTANT**, Need Key to send End Time

	FResponse_QASession() {}
};

// ====================================

UCLASS(Blueprintable, hideCategories = (Rendering, Replication, Input, Actor, "Actor Tick"))
class THEELDER_API AHttpService : public AActor
{
	GENERATED_BODY()

private :
	class AMokosh* playerRef;
	class AChi* chiRef;
	
private:
	FHttpModule* Http;
	//FString ApiBaseUrl = "http://cloudcomputeproj.appspot.com/";
	FString ApiBaseUrl = "http://localhost:9080/";

	FString AuthorizationHeader = TEXT("Authorization");
	void SetAuthorizationHash(FString Hash, TSharedRef<IHttpRequest>& Request);

	TSharedRef<IHttpRequest> RequestWithRoute(FString Subroute);
	void SetRequestHeaders(TSharedRef<IHttpRequest>& Request);

	TSharedRef<IHttpRequest> GetRequest(FString Subroute);
	TSharedRef<IHttpRequest> PostRequest(FString Subroute, FString ContentJsonString);
	void Send(TSharedRef<IHttpRequest>& Request);

	bool ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful);

	template <typename StructType>
	void GetJsonStringFromStruct(StructType FilledStruct, FString& StringOutput);
	template <typename StructType>
	void GetStructFromJsonString(FHttpResponsePtr Response, StructType& StructOutput);

	UPROPERTY()
		FTimerHandle TimerHandle_requestTimer;
public:
	AHttpService();
	virtual void BeginPlay() override;

	// ================ Player Pos =========================
	void SendPosition(FRequest_PlayerPos PlayerPos);
	void PositionResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	// ================ Chi State =========================
	void SendChiState(FRequest_ChiState State);
	void StateResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	// =============== Register Area/Puzzle ===============
	void RegisterArea(FRequest_Area NewArea);
	void AreaResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	// ================ QA Session =====================
	void SendQASession(FRequest_QASession NewQA);
	void QASessionResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	
};
