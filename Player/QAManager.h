// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "QAManager.generated.h"

// ======== area struct =============
USTRUCT()
struct FQAArea {
	GENERATED_BODY()
	UPROPERTY(EditAnywhere)FString name;
	UPROPERTY(EditAnywhere)FVector startPos;
	UPROPERTY(EditAnywhere)FVector endPos;

	FQAArea() {}

	FQAArea(FString newName, FVector start, FVector end) {
		name = newName;
		startPos = start;
		endPos = end;
	}
};

UCLASS()
class THEELDER_API AQAManager : public AActor
{
	GENERATED_BODY()
	
protected:
	TArray<FQAArea> registeredAreas;

	class AMokosh* PlayerRef;
	class AChi* ChiRef;

	FTimerHandle FT_PlayerPos;
	FTimerHandle FT_ChiPos;
	FTimerHandle FT_State;

public:	
	// Sets default values for this actor's properties
	AQAManager();

	// reference to http service created on qa start
	class AHttpService* HttpActor;

	// date time string of current session
	FString currSession;

	// index of current area struct
	int currentArea;

	// timer that counts up every tick to time session
	float sessionTimer;

	bool startTimer;
	// if true, check for start, if false check for end
	bool checkForStart;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void CreateSession(bool firstTime, FString option);
	// start timer and call send data functions
	void StartSession();

	void SendPlayerPos();

	void SendChiPos();

	void SendChiState();

	void EndSession();
};
