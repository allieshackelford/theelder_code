// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "InputCoreTypes.h"
#include "ElderPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API AElderPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	UUserWidget* mMainMenu;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Menus")
		TSubclassOf<class UUserWidget> wMainMenu;

	UUserWidget* mOptionsMenu;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Menus")
		TSubclassOf<class UUserWidget> wOptionsMenu;

	UUserWidget* mPauseMenu;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Menus")
		TSubclassOf<class UUserWidget> wPauseMenu;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reticle")
		TSubclassOf<class UCrystalCrosshair> wReticle;

	UCrystalCrosshair* mReticle;

	class UChiIndicator* mChiIndicator;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Indicator")
		TSubclassOf<class UChiIndicator> wChiIndicator;
	

public:
	AElderPlayerController(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;


	void ToggleReticleOn();

	void ToggleReticleOff();

	void ToggleReticleHittable(bool IsHittable);

	UFUNCTION(BlueprintCallable)
		void TriggerChiIndicator();

	//virtual void UpdateRotation(float DeltaTime) override;
	
	
};
